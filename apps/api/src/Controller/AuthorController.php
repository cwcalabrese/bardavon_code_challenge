<?php
namespace App\Controller;

use App\Repository\AuthorRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class AuthorController extends BaseController
{
    /**
     * @Route("/authors", methods="GET")
     */
    public function getAuthorsAction(AuthorRepository $authorRepository)
    {
        $transformed = $authorRepository->transformAll();

        return $this->respond($transformed);
    }

    /**
     * @Route("/author/{id}", methods="GET")
     */
    public function getAuthorAction($id, AuthorRepository $authorRepository)
    {
        $author = $authorRepository->find($id);

        $transformed = $authorRepository->transform($author);

        return $this->respond($transformed);
    }

    /**
     * @Route("/author/{id}/entries", methods="GET")
     */
    public function getAuthorWithEntriesAction($id, AuthorRepository $authorRepository)
    {
        $author = $authorRepository->find($id);

        $transformed = $authorRepository->transformWithEntries($author);

        return $this->respond($transformed);
    }
}