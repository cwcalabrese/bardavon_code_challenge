<?php
namespace App\Controller;

use App\Entity\Blog;
use App\Repository\BlogRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class BlogController extends BaseController
{
    /**
     * @Route("/blogs", methods="GET")
     */
    public function getBlogRollAction(BlogRepository $blogRepository)
    {
        $blogRoll = $blogRepository->transformAll();

        return $this->respond($blogRoll);
    }

    /**
     * @Route("/blog/{id}", methods="GET")
     */
    public function getBlogAction($id, BlogRepository $blogRepository)
    {
        $blog = $blogRepository->find($id);

        $transformed = $blogRepository->transform($blog);

        return $this->respond($transformed);
    }

    /**
     * @Route("/blog", methods="POST")
     */
    public function createBlog(Request $request, BlogRepository $blogRepository, CategoryRepository $categoryRepository, EntityManagerInterface $em)
    {
        $request = $this->transfromJsonBody($request);

        if (!$request) {
            return $this->respondValidationError('A valid request was not provided');
        }

        if (!$request->get('title')) {
            return $this->respondValidationError('No title was provided for the blog entry');
        }

        if (!$request->get('author')) {
            return $this->respondValidationError('No author was provided for the blog entry');
        }

        if (!$request->get('category')) {
            return $this->respondValidationError('No category was selected for the blog entry');
        }

        if (!$request->get('content')) {
            return $this->respondValidationError('No content was provided for the blog entry');
        }

        $category = $categoryRepository->find($request->get('category'));

        $blog = new Blog;
        $blog->setCreated(new \DateTime());
        $blog->setCategory($category);
        $blog->setAuthor($request->get('author'));
        $blog->setTitle($request->get('title'));
        $blog->setContent($request->get('content'));
        $em->persist($blog);
        $em->flush();

        return $this->respondCreated($blogRepository->transform($blog));
    }
}