<?php
namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CategoryController extends BaseController
{
    /**
     * @Route("/categories", methods="GET")
     */
    public function getCategoriesAction(CategoryRepository $categoryRepository)
    {
        $transformed = $categoryRepository->transformAll();

        return $this->respond($transformed);
    }

    /**
     * @Route("/category/{id}", methods="GET")
     */
    public function getCategoryAction($id, CategoryRepository $categoryRepository)
    {
        $category = $categoryRepository->find($id);

        $transformed = $categoryRepository->transform($category);

        return $this->respond($transformed);
    }

    /**
     * @Route("/category/{id}/entries", methods="GET")
     */
    public function getCategoryWithEntriesAction($id, CategoryRepository $categoryRepository)
    {
        $category = $categoryRepository->find($id);

        $transformed = $categoryRepository->transformWithEntries($category);

        return $this->respond($transformed);
    }
}