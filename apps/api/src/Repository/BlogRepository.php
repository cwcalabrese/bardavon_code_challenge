<?php

namespace App\Repository;

use App\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Blog::class);
    }

    public function transform(Blog $blog)
    {
        $blogCreated = $blog->getCreated();
        $blogUpdated = $blog->getUpdated();

        return [
            'id' => (int) $blog->getId(),
            'created' => (string) $blogCreated->format('Y-m-d H:i:s'),
            'updated' => (string) $blogUpdated->format('Y-m-d H:i:s'),
            'author' => (string) $blog->getAuthor()->getName(),
            'title' => (string) $blog->getTitle(),
            'content' => (string) $blog->getContent(),
            'category' => (string) $blog->getCategory()->getName(),
            'description' => (string) $blog->getDescription()
        ];
    }

    public function transformAll()
    {
        $blogs = $this->findAll();
        $blogArray = [];

        foreach ($blogs as $blog) {
            $blogArray[] = $this->transform($blog);
        }

        return $blogArray;
    }

    // /**
    //  * @return Blog[] Returns an array of Blog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Blog
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
