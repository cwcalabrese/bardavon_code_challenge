<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function transform(Author $author)
    {
        return [
            'id' => (int) $author->getId(),
            'name' => (string) $author->getName(),
            'bio' => (string) $author->getBio(),
        ];
    }

    public function transformWithEntries(Author $author)
    {
        $blogRepository = $this->getEntityManager()->getRepository(Blog::class);
        $entries = $author->getEntries();
        $entryArray = [];

        foreach ($entries as $entry) {
            $entryArray[] = $blogRepository->transform($entry);
        }

        return [
            'id' => (int) $author->getId(),
            'name' => (string) $author->getName(),
            'bio' => (string) $author->getBio(),
            'entries' => (array) $entryArray
        ];
    }

    public function transformAll()
    {
        $authors = $this->findAll();
        $authArray = [];

        foreach ($authors as $author) {
            $authArray[] = $this->transform($author);
        }

        return $authArray;
    }

    // /**
    //  * @return Author[] Returns an array of Author objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Author
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
