<?php
namespace App\DataFixtures;

use App\Entity\Blog;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BlogFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var array
     */
    protected $blogStub = [
        ["title" => "Eatin dem dar Spicy Taters", "description" => "Whutcher know now bout whoozerwhatsin", "content" => "Gitcher ol self now bouts gon be whert abowts uh terbel spewn iffin yagots that thar ky ann. Go out yonder tharbowts n figur wherbouts an eye gee a might well yonder be n grab yerslef summuthat thar freezer taters. Toss em whut bouts a timer twenny, leebem on a ford hood n pop a burr. Justin' bouts when that thar burr is gittin onner wayta dun, them taters prolly be fixin ta be ready.", "category" => 1, "author" => 2],
        ["title" => "Spicy Chili is the Best", "description" => "What about spicy chili makes it so intriguing", "content" => "Ah chili. The classic food of the wayward outlaw and cowpoke alike. What a menagerie of flavors and cultures, styles and ingredients, all for the savoring of the willing palette. The spicier a chili is, the more these flavors shout out to the taste buds as they cry for relief from the palpable heat.", "category" => 1, "author" => 3],
        ["title" => "A Challenge of Code", "description" => "The kind of poem a holiday rabbit might bring around", "content" => "I've built a ship to sail at see\nA complete ocean liner that serves at request\nThis ship plays the ocean it's own symfony\nBut it's deck is to high for the port to access\n\nAnother boat to fill where it lacks\nOne much more compact, but pretty to see\nIt'll be at the front and need to react\nWhen we dock 'er requests will finally complete", "category" => 2, "author" => 3],
        ["title" => "Is That Thing Moving?", "description" => "Seriously, that dark thing on the floor. Is it moving?", "content" => "You might think I'm joking around here, but I'm not.  That thing... right there... on the floor - I swear it's moving.  It really seems like it's moving right towards me too.\n\nIt better not be a spider. I swear to everything good in this world, if it's a spider? I'm outta here.", "category" => 3, "author" => 1],
    ];

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $createdOn = new \DateTime();

        foreach ($this->blogStub as $blogData) {
            $cat_tag = 'category_' . $blogData["category"];
            $auth_tag = 'author_' . $blogData["author"];

            $blog = new Blog();
            $blog->setCreated($createdOn);
            $blog->setUpdated($createdOn);
            $blog->setCategory($this->getReference($cat_tag));
            $blog->setAuthor($this->getReference($auth_tag));
            $blog->setTitle($blogData["title"]);
            $blog->setDescription($blogData["description"]);
            $blog->setContent($blogData["content"]);
            $manager->persist($blog);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            AuthorFixtures::class,
            CategoryFixtures::class
        );
    }
}