<?php
namespace App\DataFixtures;

use App\Entity\Author;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AuthorFixtures extends Fixture
{
    protected $authorsStub = [
        ["name" => "Bob Dobalina", "bio" => "He won't quit and really makes you sick"],
        ["name" => "Emmit Bodine Mayfrog", "bio" => "Wherts it gone yonder whuchernow giterdun"],
        ["name" => "Mr Wendell", "bio" => "He's got no clothes, no money and no plate"]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $x = 1;

        foreach ($this->authorsStub as $authorData) {
            $author = new Author();
            $author->setName($authorData["name"]);
            $author->setBio($authorData["bio"]);

            $ref_tag = 'author_' . $x;
            $this->addReference($ref_tag, $author);
            $manager->persist($author);

            $x = $x + 1;
        }

        $manager->flush();
    }
}