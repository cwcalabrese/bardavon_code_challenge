<?php
namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    /**
     * @var array
     */
    protected $categoryStub = [
        ["name" => "Spicy Food", "description" => "If it don't make ya pucker, then you're a sucker."],
        ["name" => "Waxing Poetic", "description" => "Words that rhyme, all the time."],
        ["name" => "Things on the Floor", "description" => "What's that thing on the floor? What is it?"]
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $x = 1;

        foreach ($this->categoryStub as $categoryData) {
            $category = new Category();
            $category->setName($categoryData["name"]);
            $category->setDescription($categoryData["description"]);

            $ref_tag = 'category_' . $x;
            $this->addReference($ref_tag, $category);
            $manager->persist($category);

            $x = $x + 1;
        }

        $manager->flush();
    }
}