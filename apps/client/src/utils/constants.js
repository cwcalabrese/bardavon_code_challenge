const ROOT_ENDPOINT = 'http://localhost:82';

//TODO: If this were a production ready application, handlebar replace service would be added to handle endpoint transforms
const CONSTANTS = {
  api: {
    endpoint: {
      root: ROOT_ENDPOINT,
      blog: ROOT_ENDPOINT + '/blog/',
      blog_roll: ROOT_ENDPOINT + '/blogs'
    }
  }
};

export default CONSTANTS;
