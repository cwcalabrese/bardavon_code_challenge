import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers/combine';
import middleware from './middleware';

const preloadedState = {};

const store = createStore(
  reducers,
  preloadedState,
  compose(
    applyMiddleware(...middleware)
  )
);

export default store;


