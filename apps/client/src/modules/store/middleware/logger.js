const logger = store => next => action => {
  let d = new Date();
  let zone = d.getTime();
  console.log('');
  console.log('//++++++++++ START: ' + zone);
  console.log('dispatching ', action);
  let result = next(action);
  console.log('next state: ', store.getState());
  console.log('//++++++++++ END: ', + zone);
  console.log('');
  return result;
};

export default logger;