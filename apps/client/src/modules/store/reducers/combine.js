import { combineReducers } from 'redux';

import blogReducer from './blogReducer';
import blogRollReducer from './blogRollReducer';

const reducers = combineReducers({
  blog_roll: blogRollReducer,
  blog: blogReducer
});

export default reducers;
