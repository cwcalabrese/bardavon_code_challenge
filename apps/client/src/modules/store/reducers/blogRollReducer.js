import TYPES from '../types/blogTypes';

const defaultState = {
  isFetching: false,
  message: null,
  data: []
};

const blogReducer = (state = defaultState, action) => {
  switch(action.type) {
    case TYPES.REQUEST_BLOG_ROLL:
      return {
        ...state,
        isFetching: true
      };
    case TYPES.RECEIVE_BLOG_ROLL:

      return action.payload ? {
        ...state,
        isFetching: false,
        message: 'success',
        data: action.payload
      } : {
        ...state,
        isFetching: false,
        message: 'No blog data found.'
      };
    default:
      return state;
  }
};

export default blogReducer;
