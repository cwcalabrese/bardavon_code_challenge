import TYPES from '../types/blogTypes';
import _ from 'lodash';

const defaultState = {
  isFetching: false,
  message: null,
  data: {}
};

const blogReducer = (state = defaultState, action) => {
  switch(action.type) {
    case TYPES.REQUEST_BLOG:
      return {
        ...state,
        isFetching: true
      };
    case TYPES.RECEIVE_BLOG:
      const blogId = action.payload ? action.payload.id : null;
      let message = 'success';

      if (_.isNull(blogId)) {
        message = 'erroneous blog data received.'
      }

      return blogId ? {
        ...state,
        isFetching: false,
        message: message,
        data: action.payload
      } : {
        ...state,
        isFetching: false,
        message: message
      };
    default:
      return state;
  }
};

export default blogReducer;
