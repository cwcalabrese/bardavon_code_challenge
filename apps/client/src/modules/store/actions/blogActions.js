import TYPES from '../types/blogTypes';
import CONSTANTS from '../../../utils/constants';
import _ from 'lodash';

import * as blogRollActions from './blogRollActions';

/*
 * Individual Blog redux fetch methods
 */
export const requestBlog = () => ({
  type: TYPES.REQUEST_BLOG
});

export const receiveBlog = (blog) => ({
  type: TYPES.RECEIVE_BLOG,
  payload: blog
});

const fetchBlog = (blog, id) => dispatch => {
  const endpoint = CONSTANTS.api.endpoint.blog + id;

  dispatch(requestBlog());

  return fetch(endpoint)
    .then(response => {
      response.json().then(json => dispatch(receiveBlog(json, id)));
    })
};

export const fetchBlogIfNeeded = (blog, id) => (dispatch, getState) => {
  if (shouldFetchBlog(blog, getState().blog, id)) {
    return dispatch(fetchBlog(blog, id));
  }
};

const shouldFetchBlog = (data, parent, id) => {
  if (parent.isFetching) {
    return false;
  }

  if (parent.checkId !== id) {
    return true;
  }

  return _.isNil(parent[id]);
};

/*
 * The following actions don't truly rely on redux to be run but the do run other redux-reliant methods, so
 * it's far easier to house them here than import them for a non-production application
 */

export const postBlog = (blogData) => (dispatch, getState) => {
  const currBlogRoll = getState().blog_roll;
  const endpoint = CONSTANTS.api.endpoint.blog;
  const options = {
    method: 'post',
    body: JSON.stringify(blogData)
  };

  return fetch(endpoint, options)
    .then(response => dispatch(blogRollActions.fetchBlogRollIfNeeded(currBlogRoll)));
};

export const deleteBlog = (blogId) => (dispatch, getState) => {
  const currBlogRoll = getState().blog_roll;
  const endpoint = CONSTANTS.api.endpoint.blog;
  const options = {
    method: 'delete',
    body: blogId
  };

  return fetch(endpoint, options)
    .then(response => dispatch(blogRollActions.fetchBlogRollIfNeeded(currBlogRoll)));
};