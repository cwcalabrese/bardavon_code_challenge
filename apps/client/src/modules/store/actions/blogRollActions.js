import TYPES from '../types/blogTypes';
import CONSTANTS from '../../../utils/constants';
import _ from 'lodash';

/*
 * Blog Roll redux fetch methods
 */

export const requestBlogRoll = () => ({
  type: TYPES.REQUEST_BLOG_ROLL
});

export const receiveBlogRoll = (blogRoll) => ({
  type: TYPES.RECEIVE_BLOG_ROLL,
  payload: blogRoll
});

const fetchBlogRoll = (blogRoll) => dispatch => {
  const endpoint = CONSTANTS.api.endpoint.blog_roll;

  dispatch(requestBlogRoll());

  return fetch(endpoint)
    .then(response => {
      response.json().then(json => dispatch(receiveBlogRoll(json)));
    });
};

export const fetchBlogRollIfNeeded = (blogRoll) => (dispatch, getState) => {
  if (shouldFetchBlogRoll(blogRoll, getState().blog_roll)) {
    return dispatch(fetchBlogRoll(blogRoll));
  }
};

const shouldFetchBlogRoll = (data, parent) => {
  if (parent.isFetching) {
    return false;
  }

  if (_.isEmpty(data)) {
    return true;
  }

  return _.isEmpty(parent.data);
};