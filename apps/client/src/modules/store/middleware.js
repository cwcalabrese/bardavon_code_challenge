import thunk from './middleware/thunk';
import logger from './middleware/logger';

const middleware = [
  thunk,
  logger
];

export default middleware;
