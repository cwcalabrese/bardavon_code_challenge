import React, { PureComponent } from 'react';
import { BeatLoader } from 'react-spinners';

/*
 * react-bootstrap's spinner isn't centering successfully, so I've just created my own.
 */

class Loader extends PureComponent {
  render = () => {
    return (
      <div className={'d-flex justify-content-center push-from-top'}>
        <BeatLoader sizeUnit={'px'} size={20} color={'#42a1c4'}/>
      </div>
    );
  }
}

export default Loader;
