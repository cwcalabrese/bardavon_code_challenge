import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
// import { Link } from 'react-router-dom';

import routes from '../../static/routes';

class Navigation extends Component {
  render = () => {

    return (
      <Navbar expand={'lg'} sticky={'top'} bg={'awesomeblue'} variant={'dark'}>
        <Navbar.Brand href={'/'}>Awesome Blog</Navbar.Brand>
        <Navbar.Collapse id={'primary-navbar'}>
          <Nav className={'ml-auto'}>
            {
              routes.map(route => {
                const disabled = route.id === 'blog-add';

                return route.name ? (
                  <Nav.Link key={route.id} href={route.path} disabled={disabled}>
                    { route.name }
                  </Nav.Link>
                ) : null;
              })
            }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default Navigation;
