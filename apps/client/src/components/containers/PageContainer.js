import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import RoutingContainer from './RoutingContainer';
import Navigation from '../elements/Navigation';

class PageContainer extends Component {
  constructor(props) {
    super(props);

    this.setActive = this.setActive.bind(this);

    this.state = {
      active: ''
    };
  }

  setActive = (path) => {
    const pathArr = path.split('/');
    console.log('PageContainer::setActive - path: ', path);
    console.log('PageContainer::setActive - pathArr: ', pathArr);
  };


  render = () => {

    return (
      <Container className={'page-container'}>
        <Navigation />
        <Row>
          <Col>
            <RoutingContainer handleActive={this.setActive} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default PageContainer;
