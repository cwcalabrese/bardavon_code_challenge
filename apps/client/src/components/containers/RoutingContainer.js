import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';

// Import our base route components
import BlogRoll from '../views/BlogRoll';
import BlogView from '../views/BlogView';
import BlogCreate from '../views/BlogCreate';

import routes from '../../static/routes';

class RoutingContainer extends Component {
  getRouteComponent = name => {
    switch(name) {
      case 'blog-roll':
        return BlogRoll;
      case 'blog-view':
        return BlogView;
      case 'blog-create':
        return BlogCreate;
      default:
        return BlogRoll;
    }
  };

  componentDidMount = () => {

  };

  componentDidUpdate = (prevProps, prevState, snapShot) => {
    if (prevProps !== this.props) {
      if (this.props.location.pathname !== prevProps.location.pathname) {
        if (typeof this.props.handleActive === 'function') {
          this.props.handleActive(this.props.location.pathname);
        } else {
          console.warn('No active path handler was found in RouteContainer');
        }
      }
    }
  };

  render = () => {

    return (
      <Switch>
        {
          routes.map(route => {
            const Component = this.getRouteComponent(route.component);
            return <Route key={route.id} exact={route.exact} path={route.path} component={Component} />
          })
        }
      </Switch>
    );
  }
}

export default withRouter(RoutingContainer);
