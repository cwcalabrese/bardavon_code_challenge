import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, ListGroup } from 'react-bootstrap';
import _ from 'lodash';

import Loader from '../elements/Loader';
import * as blogRollActions from '../../modules/store/actions/blogRollActions';
import store from '../../modules/store/store';

class BlogRoll extends Component {
  componentDidMount = () => {
    const { blogRoll } = this.props;

    if (_.isEmpty(blogRoll) || _.isNull(blogRoll)) {
      store.dispatch(blogRollActions.fetchBlogRollIfNeeded(blogRoll));
    }
  };

  render = () => {
    const { blogRoll } = this.props;
    const blogRollOrdered = _.orderBy(blogRoll, ['created'], ['desc']);

    return (blogRoll && !_.isEmpty(blogRoll)) ? (
      <Row>
        <Col>
          <Row>
            <Col>
              <h2 className={'page-title'}>All Blogs</h2>
            </Col>
          </Row>
          <Row>
            <Col>
              <ListGroup variant={'flush'}>
                {
                  blogRollOrdered.map(blog => {
                    const blogPath = '/blog/' + blog.id;

                    return (
                      <ListGroup.Item key={blog.id} action href={blogPath}>
                        <small>{ blog.created }</small>
                        <h5>{ blog.title }</h5>
                        <strong>{ blog.category }</strong>
                        <br />
                        <small><strong><em>{ blog.author }</em></strong></small>
                        <br />
                        <p>{ blog.description }</p>
                      </ListGroup.Item>
                    );
                  })
                }
              </ListGroup>
            </Col>
          </Row>
        </Col>
      </Row>
    ) : <Loader />
  }
}

const mapStateToProps = (state) => {
  const { blog_roll } = state;

  return {
    blogRoll: blog_roll.data
  }
};

export default connect(mapStateToProps)(BlogRoll);
