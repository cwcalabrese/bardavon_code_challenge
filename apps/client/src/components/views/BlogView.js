import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import _ from 'lodash';

import Loader from '../elements/Loader';
import * as blogActions from '../../modules/store/actions/blogActions';
import store from '../../modules/store/store';

class BlogView extends Component {
  componentDidMount = () => {
    const { blog, match } = this.props;
    const blogIdsDiff = (blog && !_.isEmpty(blog)) ? blog.id !== match.params.id : true;

    if (_.isEmpty(blog) || _.isNull(blog) || blogIdsDiff) {
      store.dispatch(blogActions.fetchBlogIfNeeded(blog, match.params.id));
    }
  };

  render = () => {
    const { blog } = this.props;

    const blogContent = blog && blog.content ? blog.content.split(/\r?\n/) : [];

    return blog && !_.isEmpty(blog) ? (
      <Row>
        <Col>
          <small>{ blog.created }</small>
          <h4 className={'page-title'}>{ blog.title }</h4>
          <strong>in { blog.category }</strong>
          <br />
          <small><strong><em>by { blog.author }</em></strong></small>
          <br />
          <hr />
          <br />
          {
            blogContent.map((row, index) => {

              return _.isEmpty(row) ? (
                <br key={index} />
              ) : (
                <p key={index}>{ row }</p>
              );
            })
          }
        </Col>
      </Row>
    ) : (
      <Row>
        <Col>
          <Loader />
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = state => {
  const { blog } = state;

  return {
    blog: blog.data
  }
};

export default withRouter(connect(mapStateToProps)(BlogView));
