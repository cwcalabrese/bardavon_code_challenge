const routes = [
  {
    order: 1,
    id: 'blog-roll',
    component: 'blog-roll',
    name: 'Home',
    path: '/',
    exact: true,
    index: true
  },
  {
    order: 2,
    id: 'blog-view',
    component: 'blog-view',
    name: null,
    path: '/blog/:id',
    exact: false,
    params: [
      {
        order: 1,
        name: 'id',
        required: true
      }
    ]
  },
  {
    order: 3,
    id: 'blog-add',
    component: 'blog-create',
    name: 'Add New',
    path: '/create',
    exact: true
  }
];

export default routes;
