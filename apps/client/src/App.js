import React from 'react';
import PageContainer from './components/containers/PageContainer';

import './App.css';

function App() {
  return (
    <div className={'app-container'}>
      <PageContainer />
    </div>
  );
}

export default App;
