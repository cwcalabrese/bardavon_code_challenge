#Bardavon Coding Challenge Project Setup
Symfony 4 API, Angular 7 frontend

---

## Structure

        Database:
        - MySql
        - PhpMyAdmin
        
        Server:
        - PHP
        - Apache
        
        Frond End:
        - NGINX
        - Node (React)

## Setup
Build project
```bash
$ docker-compose build
```
Start server
```bash
$ docker-compose up
```

## Install Fixtures Data
Get PHP container image
```bash
$ docker ps
```
Note 'bardavonchallenge_php' container and copy the container name and SSH into that container
```bash
$ docker exec -it {container_name} bash
```
After you've successfully SSH'd in to the container, cd to the project directory
```bash
$ cd sf4
```

Once in the sf4 directory, run the following
```bash
$ php bin\console doctrine:migrations:migrate
```
Then install the dummy fixture data
```bash
$ php bin\console doctrine:fixutres:load
```

## Project locations
Symfony: http://localhost:82

React: http://localhost:3000

PhpMyAdmin: http://localhost:8080

## Useful
If errors occur with Symfony 4, run
```bash
$ composer install
```
from the /apps/api directory

---
If errors occur with React, run
```bash
npm install
```
from the /apps/client directory

___
If docker cpu usage skyrockets, remove the .env file located at
```bash
/apps/client/.env
```
The only purpose of this environment variable is to allow React's hot module reload in docker and isn't needed unless you're actively developing on the front-end.


